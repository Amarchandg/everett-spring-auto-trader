package com.citi.training.trader;

import org.junit.Test;

import com.citi.training.trader.model.Stock;

public class StockTests {
	@Test
	public void test_Stock_fullConstructor() {
		int id = 1;
		String ticker = "AAPL";
		
		Stock stock = new Stock(id,ticker);
		assert (stock.getId() == id);
		assert (stock.getTicker()).equals(ticker);
	}
	
	@Test
	public void test_Stock_setters() {
		int id = 1;
		String ticker = "AAPL";
		
		int newId=3;
		String newTicker="GOOG";
		
		Stock testStock = new Stock(id,ticker);
		testStock.setId(newId);
		testStock.setTicker(newTicker);
		assert (testStock.getId() == newId);
		assert (testStock.getTicker()).equals(newTicker);
	}
	
	@Test
	public void test_Stock_toString() {
		int testId = 1;
		String testTicker = "AAPL";
		
		Stock testStock = new Stock(testId,testTicker);
		assert (testStock.toString() != null);
		assert (testStock.toString().contains(testStock.getTicker()));
	}


}
