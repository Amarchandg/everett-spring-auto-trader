package com.citi.training.trader;

import org.junit.Test;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;

public class TradeTests {
	@Test
	public void test_Profit_Constructor() {
		String ticker = "AAPL";
		int id = 1;
		Stock stock = new Stock(id, ticker);
		int size = 200;
		double price = 99.99;

		Strategy strategy = new Strategy(stock, size);
		Trade testTrade = new Trade(strategy.getStock(), strategy.getId(), price, strategy.getSize(),
				Trade.TradeType.BUY, strategy);

		assert (testTrade.getPrice() == price);
		assert (testTrade.getStock().equals(stock));
		assert (testTrade.getSize() == size);
	}

	@Test
	public void test_Trade_setters() {
		String ticker = "AAPL";
		int id = 1;
		Stock stock = new Stock(id, ticker);
		int size = 200;
		double price = 99.99;

		Strategy strategy = new Strategy(stock, size);
		Trade testTrade = new Trade(strategy.getStock(), strategy.getId(), price, strategy.getSize(),
				Trade.TradeType.BUY, strategy);
		
		int newId=5;
		double newPrice=199.99;
		int newStrategyId=6;
		int newSize=400;
		
		testTrade.setId(newId);
		testTrade.setPrice(newPrice);
		testTrade.setSize(newSize);
		testTrade.setStrategy_id(newStrategyId);
	
		assert (testTrade.getId() == newId);
		assert (testTrade.getPrice() == newPrice);
		assert (testTrade.getSize() == newSize);
		assert (testTrade.getStrategy_id() == newStrategyId);
	}

	@Test
	public void test_Trade_toString() {
		String ticker = "AAPL";
		int id = 1;
		Stock stock = new Stock(id, ticker);
		int size = 200;
		double price = 99.99;

		Strategy strategy = new Strategy(stock, size);
		Trade testTrade = new Trade(strategy.getStock(), strategy.getId(), price, strategy.getSize(),
				Trade.TradeType.BUY, strategy);
		assert (testTrade.toString() != null);
		assert (testTrade.toString().contains(stock.getTicker()));

	}

}
