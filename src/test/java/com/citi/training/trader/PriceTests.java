package com.citi.training.trader;

import org.junit.Test;

import static org.junit.Assert.assertSame;

import java.util.Date;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

public class PriceTests {
	@Test
	public void test_Price_Constructor() {
		int testId = -1;
		String testTicker = "AAPL";
		double price =99.99;
		
		Stock testStock = new Stock(testId,testTicker);
		Price testPrice = new Price(testStock,price);
		assert (testPrice.getId() == testId);
		assert (testPrice.getStock().equals(testStock));
		assert (testPrice.getPrice() == price);
	}
	
	@Test
	public void test_Price_setters() {
		int testId = -1;
		String testTicker = "AAPL";
		double price =99.99;
		
		Stock testStock = new Stock(testId,testTicker);
		Price testPrice = new Price(testStock,price);
		Stock newStock = new Stock(4,"GOOG");
		double newPrice=199.99;
		int newId = 3;
		
		testPrice.setId(newId);
		testPrice.setPrice(newPrice);
		testPrice.setStock(newStock);
		assert (testPrice.getId() == newId);
		assert (testPrice.getPrice() == newPrice);
		//assert (testPrice.getStock().equals(newStock));
	}
	
	@Test
	public void test_Price_toString() {
		int testId = -1;
		String testTicker = "AAPL";
		double price =99.99;
		
		Stock testStock = new Stock(testId,testTicker);
		Price testPrice = new Price(testStock,price);
		assert (testPrice.toString() != null);
		assert (testPrice.toString().contains(testPrice.getStock().toString()));
	}

}
