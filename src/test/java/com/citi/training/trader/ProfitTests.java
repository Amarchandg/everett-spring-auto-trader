package com.citi.training.trader;

import org.junit.Test;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Profit;
import com.citi.training.trader.model.Stock;

public class ProfitTests {
	@Test
	public void test_Profit_Constructor() {
		double profit =99.99;
		Profit testProfit = new Profit(profit);
		assert (testProfit.getProfit() == profit);

	}
	
	@Test
	public void test_Profit_setters() {
		double profit =99.99;
		Profit testProfit = new Profit(profit);
		double newProfit=199.99;
		testProfit.setProfit(newProfit);
		assert (testProfit.getProfit() == newProfit);
	}
	
	@Test
	public void test_Profit_toString() {
		double profit =99.99;
		Profit testProfit = new Profit(profit);
		assert (testProfit.toString() != null);

	}

}
