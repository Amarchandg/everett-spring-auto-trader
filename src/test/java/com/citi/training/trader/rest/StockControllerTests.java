package com.citi.training.trader.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.service.StockService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(StockController.class)
@ActiveProfiles("h2test")
public class StockControllerTests {
	private static final Logger logger = LoggerFactory.getLogger(StockControllerTests.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private StockService mockStockService;
	
    @Test
    public void findAllStocks_returnsList() throws Exception {
        when(mockStockService.findAll())
            .thenReturn(new ArrayList<Stock>());

        this.mockMvc
                .perform(get("/stock/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();
    }
    
    @Test
    public void findStockById_returnsStock() throws Exception {
        int id=1;
        when(mockStockService.findById(id))
        	.thenReturn(new Stock(id,"GOOG"));
        
        this.mockMvc
		        .perform(get("/stock/" + id))
		        .andExpect(status().isOk())
		        .andExpect(jsonPath("$.size()").isNumber())
		        .andReturn();
    }
    
    @Test
    public void createStock_returnsCreated() throws Exception {
        Stock testStock = new Stock(2, "C");

        when(mockStockService.create(any(Stock.class)))
            .thenReturn(testStock.getId());

        this.mockMvc.perform(
                post("/stock/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testStock)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id")
                    .value(testStock.getId()))
                .andReturn();
    }
    
    @Test
    public void deleteStock_returnsNotFound() throws Exception {    
        this.mockMvc
		        .perform(delete("/stock/" + 11))
		        .andExpect(status().isNoContent())
		        .andReturn();
    }
    
    
}
