package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;

/**
 * Integration test for Trade REST Interface.
 *
 * 
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
public class TradeControllerIT {

	private static final Logger logger = LoggerFactory.getLogger(TradeControllerIT.class);

	@Autowired
	private TestRestTemplate restTemplate;

	@Value("${com.citi.trading.trader.rest.stock-base-path:/trade}")
	private String tradeBasePath;

	@Value("${com.citi.trading.trader.rest.stock-base-path:/stock}")
	private String stockBasePath;

	@Test
	@Transactional
	public void findAll_returnsList() {
		double price = 99.99;
		int size = 200;
		Stock testStock = new Stock(1, "AAPL");
		Strategy strategy = new Strategy(testStock, size);
		Trade testTrade = new Trade(strategy.getStock(), strategy.getId(), price, strategy.getSize(),
				Trade.TradeType.SELL, strategy);
		// Need to insert stock because of foreign key
		restTemplate.postForEntity(stockBasePath, testStock, Stock.class);
		restTemplate.postForEntity(tradeBasePath, testTrade, Trade.class);

		ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(tradeBasePath, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Trade>>() {
				});
		assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
		assert (findAllResponse.getBody().get(0).getPrice() == price);
		assert (findAllResponse.getBody().get(0).getSize() == size);
		assert (findAllResponse.getBody().get(0).getStock().getTicker().equals(testStock.getTicker()));
	}

	@Test
	@Transactional
	public void createTrade_test() {
		double price = 99.99;
		int size = 200;
		Stock testStock = new Stock(8, "DIS");
		Strategy strategy = new Strategy(testStock, size);
		Trade testTrade = new Trade(strategy.getStock(), strategy.getId(), price, strategy.getSize(),
				Trade.TradeType.SELL, strategy);
		testTrade.setId(20);
		// Need to insert stock because of foreign key
		restTemplate.postForEntity(stockBasePath, testStock, Stock.class);
		restTemplate.postForEntity(tradeBasePath, testTrade, Trade.class);
		
        ResponseEntity<Trade> createdResponse =
                restTemplate.postForEntity(tradeBasePath,
                                           testTrade, Trade.class);
        
        assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);
	}
	
	@Test
	@Transactional
	public void createTradeAndDelete_test() throws TradeNotFoundException {
		double price = 99.99;
		int size = 200;
		Stock testStock = new Stock(8, "DIS");
		Strategy strategy = new Strategy(testStock, size);
		Trade testTrade = new Trade(strategy.getStock(), strategy.getId(), price, strategy.getSize(),
				Trade.TradeType.SELL, strategy);
		testTrade.setId(20);
		// Need to insert stock because of foreign key
		restTemplate.postForEntity(stockBasePath, testStock, Stock.class);
		restTemplate.postForEntity(tradeBasePath, testTrade, Trade.class);
		
        ResponseEntity<Trade> createdResponse =
                restTemplate.postForEntity(tradeBasePath,
                                           testTrade, Trade.class);
        
        //assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);
        restTemplate.delete(tradeBasePath + "/20");
        
		ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(tradeBasePath, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Trade>>() {
				});
        
	}

}
