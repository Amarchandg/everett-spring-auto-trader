package com.citi.training.trader.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(TradeController.class)
@ActiveProfiles("h2test")
public class TradeControllerTests {
	private static final Logger logger = LoggerFactory.getLogger(TradeControllerTests.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TradeService mockTradeService;
	
    @Test
    public void findAllTrades_returnsList() throws Exception {
        when(mockTradeService.findAll())
            .thenReturn(new ArrayList<Trade>());

        this.mockMvc
                .perform(get("/trade/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();
    }
    
    @Test
    public void findAllTradesById_returnsList() throws Exception {
        int id=1;
    	when(mockTradeService.findAllById(id))
            .thenReturn(new ArrayList<Trade>());

        this.mockMvc
                .perform(get("/trade/" + id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();
    }
    
    @Test
    public void createStock_returnsCreated() throws Exception {
		String ticker = "AAPL";
		int id = 1;
		Stock stock = new Stock(id, ticker);
		int size = 200;
		double price = 99.99;

		Strategy strategy = new Strategy(stock, size);
		Trade testTrade = new Trade(strategy.getStock(), strategy.getId(), price, strategy.getSize(),
				Trade.TradeType.BUY, strategy);

        when(mockTradeService.save(any(Trade.class)))
            .thenReturn(testTrade.getId());

        this.mockMvc.perform(
                post("/trade/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id")
                    .value(testTrade.getId()))
                .andReturn();
    }

}
