package com.citi.training.trader.dao.mysql;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
@Transactional

public class MysqlTradeDaoTests {
	
		@SpyBean
		JdbcTemplate tpl;
		
		@Autowired
		MysqlTradeDao TradeDao;
	@Test(expected=TradeNotFoundException.class)
	public void test_saveAndDeleteById() {

		String ticker = "AAPL";
		int id = 1;
		Stock stock = new Stock(id, ticker);
		int size = 200;
		double price=99.99;
		Strategy strategy = new Strategy(stock, size);
		Trade testTrade = new Trade(strategy.getStock(), strategy.getId(), price, strategy.getSize(),
				Trade.TradeType.BUY, strategy);
		TradeDao.save(testTrade);
		TradeDao.deleteById(id);
		TradeDao.findById(id);
	}
}
