package com.citi.training.trader;

import java.util.Date;

import org.junit.Test;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Strategy;

public class StrategyTests {
	@Test
	public void test_Strategy_Constructor() {
		final int DEFAULT_MAX_TRADES = 20;
		final int DEFAULT_ID = -1;

		String ticker = "AAPL";
		int id = 1;
		Stock stock = new Stock(id, ticker);
		int size = 200;
		int currentPosition = 0;
		double lastTradePrice = 0;
		double profit = 0;

		Strategy Strategy = new Strategy(stock, size);
		assert (Strategy.getId() == DEFAULT_ID);
		assert (Strategy.getStock().equals(stock));
		assert (Strategy.getSize() == size);
		assert (Strategy.getExitProfitLoss() == DEFAULT_MAX_TRADES);
		assert (Strategy.getCurrentPosition() == currentPosition);
		assert (Strategy.getLastTradePrice() == lastTradePrice);
		assert (Strategy.getProfit() == profit);
	}

	@Test
	public void test_Strategy_fullConstructor() {
		String ticker = "AAPL";
		int id = 1;
		Stock stock = new Stock(id, ticker);
		int size = 200;
		int exitProfitLoss = 2;
		int currentPosition = 0;
		double lastTradePrice = 0;
		double profit = 0;
		Date dateStopped = new Date();

		Strategy testStrategy = new Strategy(id, stock, size, exitProfitLoss, currentPosition, lastTradePrice, profit,
				dateStopped);

		assert (testStrategy.getId() == id);
		assert (testStrategy.getStock().equals(stock));
		assert (testStrategy.getSize() == size);
		assert (testStrategy.getExitProfitLoss() == exitProfitLoss);
		assert (testStrategy.getCurrentPosition() == currentPosition);
		assert (testStrategy.getLastTradePrice() == lastTradePrice);
		assert (testStrategy.getProfit() == profit);
	}

	@Test
	public void test_Strategy_setters() {
		String ticker = "AAPL";
		int id = 1;
		Stock stock = new Stock(id, ticker);
		Stock newStock = new Stock(5, "GOOG");
		int newSize = 200;
		double newExitProfitLoss = 1.5;
		int newCurrentPosition = 1;
		double newLastTradePrice = 87.5;
		double newProfit = 1250;

		Strategy testStrategy = new Strategy(stock, newSize);
		testStrategy.setId(id);
		testStrategy.setStock(newStock);
		testStrategy.setSize(newSize);
		testStrategy.setExitProfitLoss(newExitProfitLoss);
		testStrategy.setCurrentPosition(newCurrentPosition);
		testStrategy.setLastTradePrice(newLastTradePrice);
		testStrategy.setProfit(newProfit);

		assert (testStrategy.getId() == id);
		assert (testStrategy.getStock().equals(newStock));
		assert (testStrategy.getSize() == newSize);
		assert (testStrategy.getExitProfitLoss() == newExitProfitLoss);
		assert (testStrategy.getCurrentPosition() == newCurrentPosition);
		assert (testStrategy.getLastTradePrice() == newLastTradePrice);
		assert (testStrategy.getProfit() == newProfit);
	}

	@Test
	public void test_Strategy_toString() {
		String ticker = "AAPL";
		int id = 1;
		Stock stock = new Stock(id, ticker);
		int size = 200;

		Strategy testStrategy = new Strategy(stock, size);
		assert (testStrategy.toString() != null);
		assert (testStrategy.toString().contains(stock.getTicker()));
	}

}
