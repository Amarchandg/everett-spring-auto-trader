package com.citi.training.trader.strategy;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;

@Component
public class TwoMovingAverageAlgorithm implements StrategyAlgorithm {

	private static final Logger logger = LoggerFactory.getLogger(TwoMovingAverageAlgorithm.class);

	@Autowired
	private PriceDao priceDao;

	@Autowired
	private TradeSender tradeSender;

	@Autowired
	private StrategyDao strategyDao;
	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
	public void run() {
		double movingAverage = 0, shortAverage = 0;
		boolean aboveAverage,takingPosition=false;

		for (Strategy strategy : strategyDao.findAll()) {
			if (strategy.getStopped() != null) {
				if (strategy.hasPosition()) {
					if(strategy.hasShortPosition()) {
						makeTrade(strategy, Trade.TradeType.BUY);
					}else {
						makeTrade(strategy, Trade.TradeType.SELL);
					}
					logger.debug("Strategy terminated with open position. Exiting position. Strategy " + strategy);
					strategy.closePosition();
					strategyDao.save(strategy);
				}
				continue;
			}

			if (!strategy.hasPosition()) {
				// we have no open position

				// get latest two prices for this stock
				List<Price> prices = priceDao.findLatest(strategy.getStock(), 10);

				if (prices.size() < 10) {
					logger.warn("Unable to execute strategy, not enough price data: " + strategy);
					continue;
				}

				//logger.debug("Taking position based on prices:");
				for (Price price : prices) {
				//	logger.debug(price.toString());
				}

				movingAverage = calculateAverage(5, 9, prices);
				shortAverage = calculateAverage(3, 7, prices);
				aboveAverage = movingAverage < shortAverage;
				
				for (int i = 0; i < 4; i++) {
					movingAverage = calculateAverage(5, 8-i, prices);
					shortAverage = calculateAverage(3, 6-i, prices);
					//logger.debug("Index " +i + " Moving average is " + movingAverage + " Short average is " + shortAverage);
					if (takingPosition) {
						continue;
					}
					if (aboveAverage && (movingAverage > shortAverage)){
						takingPosition=true;
						logger.debug( strategy.getId() + "Short average went beloe the moving line. Taking short position for strategy: " + strategy);
						strategy.takeShortPosition();
						strategy.setLastTradePrice(makeTrade(strategy, Trade.TradeType.SELL));
					}else if (!aboveAverage &&(movingAverage < shortAverage)) {
						takingPosition=true;
						logger.debug(strategy.getId() +" Short average went above moving average. Taking long position for strategy: " + strategy);
						strategy.takeLongPosition();
						strategy.setLastTradePrice(makeTrade(strategy,Trade.TradeType.BUY));
					}
					
				}

			} else if (strategy.hasLongPosition() && checkExitPosition(strategy)) {
				// we have a long position on this stock
				// close the position by selling
				logger.debug("Closing long position for strategy: " + strategy);
				makeTrade(strategy, Trade.TradeType.SELL);
				strategy.closePosition();

			} else if (strategy.hasShortPosition() && checkExitPosition(strategy)) {
				// we have a short position on this stock
				// close the position by buying
				logger.debug("Closing short position for strategy: " + strategy);
				makeTrade(strategy, Trade.TradeType.BUY);
				strategy.closePosition();
			}
			strategyDao.save(strategy);
		}
	}

	private double calculateAverage(double size, int startIndex, List<Price> prices) {
		double sum = 0;
		for (int i = 0; i < size; i++) {
			sum += prices.get(startIndex - i).getPrice();
		}
		return sum / size;
	}

	private double makeTrade(Strategy strategy, Trade.TradeType tradeType) {
		Price currentPrice = priceDao.findLatest(strategy.getStock(), 1).get(0);
		tradeSender.sendTrade(
				new Trade(strategy.getStock(), strategy.getId(), currentPrice.getPrice(), strategy.getSize(), tradeType, strategy));
		return currentPrice.getPrice();
	}
	
	private boolean checkExitPosition(Strategy strategy) {
		double exitPercentage =strategy.getExitProfitLoss();
		double startingPrice=strategy.getLastTradePrice();
		double currentPrice = priceDao.findLatest(strategy.getStock(), 1).get(0).getPrice();
		double percentage = (Math.abs((startingPrice - currentPrice))/startingPrice)*100;
		return percentage > exitPercentage;

	}

}
