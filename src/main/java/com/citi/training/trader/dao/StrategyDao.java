package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.Strategy;

public interface StrategyDao {

    List<Strategy> findAll();

    int save(Strategy strategy);
    
    void updateProfit(double profit,int id);
    
    void create(Strategy simplestrategy);
    
    void stopStrategy(int id);

	Strategy findById(int id);
}
