package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Strategy;

/**
 * JDBC MySQL DAO implementation for strategy table.
 *
 */
@Component
public class MysqlStrategyDao implements StrategyDao {

	private static final Logger logger = LoggerFactory.getLogger(MysqlStrategyDao.class);

	private static String FIND_ALL_SQL = "select strategy.id as strategy_id, stock.id as stock_id, stock.ticker, "
			+ "size, exit_profit_loss, current_position, last_trade_price, profit, stopped "
			+ "from strategy left join stock on stock.id = strategy.stock_id";

	private static String INSERT_SQL = "INSERT INTO strategy (stock_id, size, exit_profit_loss, current_position, "
			+ "last_trade_price, stopped) " + "values (:stock_id, :size, :exit_profit_loss, :current_position, "
			+ ":last_trade_price, :stopped)";

	private static String UPDATE_SQL = "UPDATE strategy set stock_id=:stock_id, size=:size, "
			+ "exit_profit_loss=:exit_profit_loss, current_position=:current_position, "
			+ "last_trade_price=:last_trade_price, stopped=:stopped where id=:id";

	private static String FIND_SQL = FIND_ALL_SQL + " where strategy.id = ?";
	
	private static String UPDATE_PROFIT_SQL = "UPDATE strategy set profit=:profit where id=:id";

	private static String STOP_STRATEGY_SQL = "UPDATE strategy set stopped=:stopped where id=:id";

	@Autowired
	private JdbcTemplate tpl;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public List<Strategy> findAll() {
		logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
		return tpl.query(FIND_ALL_SQL, new StrategyMapper());
	}
	
    @Override
    public Strategy findById(int id) {
        logger.debug("findBydId(" + id + ") SQL: [" + FIND_SQL + "]");
        List<Strategy> strategys = this.tpl.query(FIND_SQL,
                new Object[]{id},
                new StrategyMapper()
        );
        if(strategys.size() <= 0) {
            String warnMsg = "Requested strategy not found, id: " + id;
            logger.warn(warnMsg);
            //throw new TradeNotFoundException(warnMsg);
        }
        if(strategys.size() > 1) {
            //logger.warn("Found more than one Trade with id: " + id);
        }
        return strategys.get(0);
    }

	public int save(Strategy strategy) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

		namedParameters.addValue("stock_id", strategy.getStock().getId());
		namedParameters.addValue("size", strategy.getSize());
		namedParameters.addValue("exit_profit_loss", strategy.getExitProfitLoss());
		namedParameters.addValue("current_position", strategy.getCurrentPosition());
		namedParameters.addValue("last_trade_price", strategy.getLastTradePrice());
		// namedParameters.addValue("profit", strategy.getProfit());
		namedParameters.addValue("stopped", strategy.getStopped());

		if (strategy.getId() <= 0) {
			logger.debug("Inserting Strategy: " + strategy);

			KeyHolder keyHolder = new GeneratedKeyHolder();

			namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
			strategy.setId(keyHolder.getKey().intValue());
		} else {
			logger.debug("Updating Strategy: " + strategy);
			namedParameters.addValue("id", strategy.getId());
			namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);
		}

		logger.debug("Saved trade: " + strategy);
		return strategy.getId();
	}

	public void updateProfit(double profit, int id) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("id", id);
		namedParameters.addValue("profit", profit);
		logger.debug("Updating Strategy profit");
		namedParameterJdbcTemplate.update(UPDATE_PROFIT_SQL, namedParameters);

	}

	/**
	 * private internal class to map database rows to Strategy objects.
	 *
	 */
	private static final class StrategyMapper implements RowMapper<Strategy> {
		public Strategy mapRow(ResultSet rs, int rowNum) throws SQLException {
			logger.debug("Mapping strategy result set row num [" + rowNum + "], id : ["
					+ rs.getInt("strategy_id") + "]");

			return new Strategy(rs.getInt("strategy_id"),
					new Stock(rs.getInt("stock_id"), rs.getString("stock.ticker")), rs.getInt("size"),
					rs.getDouble("exit_profit_loss"), rs.getInt("current_position"), rs.getDouble("last_trade_price"),
					rs.getDouble("profit"), rs.getDate("stopped"));
		}
	}

	@Override
	public void stopStrategy(int id) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("id", id);
		namedParameters.addValue("stopped", new Date());
		logger.debug("Stopping strategy");
		namedParameterJdbcTemplate.update(STOP_STRATEGY_SQL, namedParameters);
	}


    @Override
    public void create(Strategy strategy) {
    	logger.info("Creating Strategy:" + strategy);
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

		namedParameters.addValue("stock_id", strategy.getStock().getId());
		namedParameters.addValue("size", strategy.getSize());
		namedParameters.addValue("exit_profit_loss", strategy.getExitProfitLoss());
		namedParameters.addValue("current_position", strategy.getCurrentPosition());
		namedParameters.addValue("last_trade_price", strategy.getLastTradePrice());
		namedParameters.addValue("profit", strategy.getProfit());
		namedParameters.addValue("stopped", strategy.getStopped());
		KeyHolder keyHolder = new GeneratedKeyHolder();
		namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
		
    	logger.info("Strategy Created:" + keyHolder.getKey());

    }
	
}
