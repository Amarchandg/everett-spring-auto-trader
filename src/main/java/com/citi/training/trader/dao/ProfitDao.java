package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.Profit;

public interface ProfitDao {
	
	Profit findById(int id);
}


