package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.ProfitDao;
import com.citi.training.trader.model.Profit;

@Component
public class MysqlProfitDao implements ProfitDao {
	
    private static final Logger logger =
            LoggerFactory.getLogger(MysqlPriceDao.class);
    
    //private static String CALCULATE_PROFIT_SQL="SELECT sum( CASE when t.buy = 0 THEN t.price*size else t.price*size * -1 END) AS Profit FROM trade as t where state = \"FILLED\";";
    	//add ID here	
    
    private static String CALCULATE_PROFIT_SQL="SELECT sum( CASE when t.buy = 0 THEN t.price*size else t.price*size * -1 END) AS Profit FROM trade as t where state = \"FILLED\" and strategy_id = ?";
    @Autowired
    private JdbcTemplate tpl;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public Profit findById(int id) {
        return tpl.query(CALCULATE_PROFIT_SQL,
        		new Object[]{id},
                new ProfitMapper()).get(0);
	}
	
	private static final class ProfitMapper implements RowMapper<Profit>{
		public Profit mapRow(ResultSet rs, int rowNum) throws SQLException{
			return new Profit(rs.getDouble("Profit"));
					//(rs.getInt("id"),
					//rs.getInt("stockId"),
					//rs.getDouble("profit"));		
		}
	}
    
}
