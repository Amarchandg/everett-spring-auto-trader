package com.citi.training.trader.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Strategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.ProfitService;
import com.citi.training.trader.service.StockService;
import com.citi.training.trader.service.TradeService;

/**
 * Service class that will listen on JMS queue for Trades and put returned
 * trades into the database.
 *
 */
@Component
public class TradeReceiver {

	private static final Logger logger = LoggerFactory.getLogger(TradeReceiver.class);

	@Autowired
	private PriceDao priceDao;
	
	@Autowired
	private TradeDao tradeDao;

	@Autowired
	private TradeSender tradeSender;

	@Autowired
	private TradeService tradeService;

	@Autowired
	private StockService stockService;

	@Autowired
	private ProfitService profitService;

	@Autowired
	private StrategyDao strategyDao;

	@JmsListener(destination = "${jms.sender.responseQueueName:OrderBroker_Reply}")
	public void receiveTrade(String xmlReply) {

		logger.debug("Processing OrderBroker Replies");

		Trade tradeReply = Trade.fromXml(xmlReply);
		logger.debug("XML reply is " + xmlReply);
		logger.debug("Parsed returned trade: " + tradeReply);
		tradeReply.setStock(stockService.findByTicker(tradeReply.getTempStockTicker()));
		int strategyId= tradeDao.findById(tradeReply.getId()).getStrategy_id();
		Strategy strategy=strategyDao.findById(strategyId);
		if (xmlReply.contains("<result>REJECTED</result>")) {
			tradeReply.stateChange(Trade.TradeState.REJECTED);
			tradeService.save(tradeReply);
			Price currentPrice = priceDao.findLatest(tradeReply.getStock(), 1).get(0);
			Trade tradeReplay =new Trade(tradeReply.getStock(), strategyId, currentPrice.getPrice(), tradeReply.getSize(), tradeReply.getTradeType(), strategy);
			tradeSender.sendTrade(tradeReplay);
			logger.info("Trade rejected. Submitting new order" +  tradeReplay);
		} else if (xmlReply.contains("<result>Partially_Filled</result>")) {
			tradeReply.stateChange(Trade.TradeState.INIT);
			tradeService.save(tradeReply);
		} else {
			tradeReply.stateChange(Trade.TradeState.FILLED);
			tradeService.save(tradeReply);

			if (!strategy.hasPosition()) {
				//StrategyId isnt always returned from the sender
				double profit = profitService.findById(strategyId).getProfit();
				logger.debug("Stocks sold, recalculating profit " + profit + "Strategy is " + strategy);
				strategyDao.updateProfit(profit , strategyId);
			}
		}
	}
}
