package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;


import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.Strategy;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/strategy")
public class SimpleStrategyController {

	private static final Logger logger = LoggerFactory.getLogger(SimpleStrategyController.class);

	@Autowired
	private StrategyDao SimpleStrategyService;

    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Strategy> findAll() {
        return SimpleStrategyService.findAll();
    }
    
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void create(@RequestBody Strategy strategy) {
		SimpleStrategyService.save(strategy);
	}
    
	@RequestMapping(value = "/stop/{id}", method = RequestMethod.POST)
	public void stopById(@PathVariable int id) {
		SimpleStrategyService.stopStrategy(id);
	}

}
