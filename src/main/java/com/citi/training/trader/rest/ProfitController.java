package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.Profit;
import com.citi.training.trader.service.ProfitService;

@RestController
@RequestMapping("/profit")
public class ProfitController {

	private static final Logger logger = LoggerFactory.getLogger(TradeController.class);

	@Autowired
	private ProfitService profitService;

    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Profit findById(int id) {
        return profitService.findById(id);
    }

}
