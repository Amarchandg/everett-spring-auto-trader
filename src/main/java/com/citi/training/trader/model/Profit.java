package com.citi.training.trader.model;

public class Profit {
    private double profit;
    
    public Profit(double profit) {
    	this.profit=profit;
    }
	
	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}
}
