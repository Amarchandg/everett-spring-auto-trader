package com.citi.training.trader.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.ProfitDao;
import com.citi.training.trader.model.Profit;

@Component
public class ProfitService {
    private static final Logger logger =
            LoggerFactory.getLogger(ProfitService.class);
    
    @Autowired
    private ProfitDao profitDao;
    
    public Profit findById(int id) {
    	return profitDao.findById(id);
    }
}
