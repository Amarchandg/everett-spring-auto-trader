#  Everett Algorithmic Trading Application

For your final application-development project, you will work in teams of three to complete and then to improve an application that supports high-frequency stock traders in managing sets of automated/algorithmic trading agents.

There is a list of mandatory tasks, including fixes to known issues with the application and creation of a CI/CD infrastructure that lets us deploy the application to an OpenShift server; and then you can either choose from among suggested application enhancements or propose your own. You will also work with a delegate from production support to complete deployment of the application to UAT and production servers.

You will demonstrate your completed and deployed application at the Tech Fair on the final day of the training program, and will give a presentation showing your command of the application's architecture and supporting technology and methodology.

Instructors will provide support and will also act as your client or customer. You will meet at least once with an instructor for advice on development strategies and technique, and once in order to review feature choices and UI design.

## Background

In a traditional market-analyst workstation, the user monitors stock prices and other data, and manually triggers trades: some number of shares of some stock, taking either a "long" position by buying the stock in hopes of selling it at a higher price or "shorting" the stock by selling it and planning to buy it back at a lower price. In our algorithmic trading workstation, the user instead creates automated traders that have certain marching orders to buy and sell on their own, and then monitors their activity, often stopping unprofitable traders, creating new ones, and tuning operating parameters.

Once activated, an algorithmic trader will trade more frequently than a human trader who makes each buy/sell decision and triggers each trade manually. For purposes of developing, testing, and especially demonstrating our application, we speed things up even further: stock pricing is available at 15-second intervals, and a trader might make a trade as frequently as once every minute or so.

There are many well-known trading algorithms, and others can easily be invented. Reliably profitable algorithms, of course, are fewer and farther between. The starter skeleton has an extremely simple "dumb" algorithm that you will need to replace with something more sophisticated.

## Environment Variables

The Application uses several available environment variables that can be changed when ran

* trade-app variables
    * DB_HOST this variable sets what database the application will use
    * DB_PASS database password
    * DB_SCHEMA sets what schema the application will use
    * MQ_HOST sets the order broker
    * MQ_PORT sets the port for the order broker
* trade-mysql variables
    *MYSQL_ROOT_PASSWORD this sets the password for the Mysql container

## Architecture

The application manages a relational database in which strategies, positions, and trades are stored and updated. Pricing history for stocks is also stored as it is used by running strategies, and to support auditing and analytics after the fact.

The strategy, two moving algorithm decides when to open and close positions based on the prices provided by the price feed. A long average and a short average are calculated based on the prices and if the short average goes above or below the long 
average when there is no open position the algorithm opens a new position by placing an order to the mockBroker. Positions are closed by comparing the price that stocks are bought at with their current prices. If the profit or loss exceeds the value
inputted by the user, then the position is closed by the algorithm. 

The trade receiver handles all responses from the mockBroker. The receiver stores all the trade responses in the database. Rejected trades are handed by writing to the database so  there is a record of a trade being rejected and then having a new 
trade with an updated price resent to the mockBroker. This ensures that for any number of rejected trades there will still always be a buy for every sell. Profit/Loss are recalculated whenever a nonrejected trade is sent to the receiver with a 
corresponding strategy with no open positions.


## External Services

The application relies on two external services, which are considered to be out of our control: a **PriceService** that provides a stream of stock pricing data on demand, and an **OrderBroker** that acts as our intermediary to the stock market itself (though it is actually not an intermediary but rather a simulation of market responses to our trade requests). The price service is a synchronous, HTTP request/response service; and the order broker is available over an asynchronous-messaging broker.

### The Mock OrderBroker
The mock order broker is a java application that listens on a JMS queue for requests to buy or sell stock. The broker will the "mock" a response to the request.

```xml
  <trade>
      <id>1</id>
      <price>99.99</price>
      <size>600</size>
      <whenAsDate>2019-07-25T02:31:07</whenAsDate>
      <stock>AAPL</stock>
      <buy>true</buy>
    </trade>
```

All fields are required.

The mock broker will also look for a JMS correlation ID, and bases response messaging on that:

If there is a correlation ID, the broker will set the same ID in a response message. This allows your application to know which request the response corresponds to.
If there is no correlation ID, the broker logs a warning and doesn't send a response message. This allows for one-way trade requests as an option, perhaps for manual traders -- but isn't appropriate for your application.
The response from the broker will be of the same XML format as above, except that there will be one additional property called "result" with one of the following values:

* FILLED -- trade executed as requested
* PARTIALLY_FILLED -- trade executed with some number of shares less than requested -- and you can check the size property for the number
* REJECTED -- the trade was not executed

The mock order broker responses may be practically immediate, or may take up to 10 seconds, and actual time will vary, message-by-message, simulating real-world slowdowns.

### Price Feed
A mock price feed service is running in the development environment at http://feed.conygre.com:8080/MockYahoo/quotes.csv

This service will return mock stock prices in response to HTTP GET requests.

An example of a request that retrieves the price of a single stock (goog):

```http://feed.conygre.com:8080/MockYahoo/quotes.csv?s=goog&f=p0```

An example of a request that retrieves the ticker, price and volume of two stocks (apl,msft):

```http://feed.conygre.com:8080/MockYahoo/quotes.csv?s=apl,msft&f=s0p0v0```

The s parameter should contain a comma separated list of stock tickers.

The f parameter should contain a combination of:

* price as p0
* ask/bid as a0/b0
* ticker as s0
* volume as v0

If you repeat requests to these URLs you should see that the price varies -- in fact it oscillates over a sine wave based on a hard-coded base price for each of a handful of securities (AAPL GOOG BRK-A NSC MSFT). If you request a ticker not known to the service, the base price will be 100.

All prices vary over a roughly two-minute sine wave, with a magnitude of 5% of the base share price; this is modulated with a shorter and shallower wave just to make the numbers look a bit more real. So, a two-moving-averages strategy set with long and short periods in the vicinity of one minute and 15 seconds, for example, should trigger regular trading.

It should not be necessary, but if you wish to deploy the Fake feed locally, you can ask the instructors for a WAR file (note that it requires Tomcat NOT JBoss/WildFly).

The source code for the fake feed is also available, so you could change it to work for different algorothms and stocks, and then incorporate it into your own project.

Bear in mind that the mock feed service is a simulation of price variations, when preparing a demo you should take into account that the prices may not vary realistically. As the project progresses you may choose to use a live price feed from the internet.

# Improvements

1. Implement more algorithms that can be run by the user
2. Create a tradeReplayer to handle bad trade statuses instead of having the trade receiver perform this. Could also expand on this tradeReplayer to handle more statuses such as settlement of trades, partial fills, and no responses from the mockBroker. 
3. Schedule a SQL script that would trim unnecessary prices being stored in the database.

